#!/usr/bin/env bash

httpstatus="000"
printf "Waiting for server to start\n"

while [ $httpstatus -ne "200" ]; do
   httpstatus=$(curl -s -o /dev/null -I -w "%{http_code}" http://couchbase-db:8091/ui/index.html)
done

sleep 30

cd /opt/couchbase-sync-gateway/bin && ./sync_gateway /config/config.json &

while true; do sleep 1000; done