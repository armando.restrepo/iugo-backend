#!/usr/bin/env bash

cd / && ./entrypoint.sh couchbase-server &

httpstatus="000"

printf "Waiting for database server to start\n"
while [ $httpstatus -ne "301" ]; do
  httpstatus=$(curl -s -o /dev/null -I -w "%{http_code}" http://127.0.0.1:8091/)
done

bucketstatus=$(curl -s -o /dev/null -I -w "%{http_code}" http://127.0.0.1:8091/pools/default/buckets/$DB_BUCKET)

if [ $bucketstatus != 200 ]
  then

  printf "Setup index and memory quota\n"
  curl -s -X POST http://127.0.0.1:8091/pools/default -d memoryQuota=512 -d indexMemoryQuota=512

  printf "Setup services\n"
  curl -s -X POST http://127.0.0.1:8091/node/controller/setupServices -d services=kv%2Cn1ql%2Cindex

  printf "Setup admin user\n"
  curl -s -X POST http://127.0.0.1:8091/settings/web -d port=8091 -d username=$CB_USER -d password=$CB_PASS

  printf "\nCreating db bucket\n"

  curl  -s -X POST -u $CB_USER:$CB_PASS \
        -d name=$DB_BUCKET -d ramQuotaMB=512 -d authType=sasl \
        http://127.0.0.1:8091/pools/default/buckets

  #   printf "\nCreating sync bucket\n"

	# curl -s -X POST -u $CB_USER:$CB_PASS \
	# -d name=$DB_SYNC_BUCKET -d ramQuotaMB=512 -d authType=sasl \
	# http://127.0.0.1:8091/pools/default/buckets
fi

printf "Database server ready\n"

while true; do sleep 1000; done