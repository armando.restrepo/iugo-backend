FROM couchbase/sync-gateway
RUN mkdir config
COPY ./config.json /config
COPY ./wait_dependency.sh /config
RUN chmod +x /config/wait_dependency.sh
ENTRYPOINT ["/bin/bash", "/config/wait_dependency.sh"]